package id.test.myapplication.Adapter

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import id.test.myapplication.DataModel.DataResponseModel
import id.test.myapplication.R

class DataListAdapter (private val mContext: Context, private val itemsList: ArrayList<DataResponseModel>?) :
    RecyclerView.Adapter<DataListAdapter.SingleItemRowHolder>() {
    private var listData : MutableList<DataResponseModel>?=itemsList

    override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): SingleItemRowHolder {
        val v = LayoutInflater.from(viewGroup.context).inflate(R.layout.data_layout, null)

        return SingleItemRowHolder(v)
    }

    override fun onBindViewHolder(holder: SingleItemRowHolder, i: Int) {

        val singleItem = listData!![i]
        Log.e("ItemDone",singleItem.fact)
        holder.tvText.text = singleItem.fact
        holder.tvText2.text = "Length ${singleItem.length}"
        holder.itemView.setOnClickListener {
            Toast.makeText(mContext,singleItem.fact,Toast.LENGTH_SHORT).show()
        }

    }

    override fun getItemCount(): Int {
        return itemsList?.size ?: 0
    }

    inner class SingleItemRowHolder(view: View) : RecyclerView.ViewHolder(view) {

        var tvText: TextView = view.findViewById(R.id.tvText)
        var tvText2: TextView = view.findViewById(R.id.tvText2)

    }

}