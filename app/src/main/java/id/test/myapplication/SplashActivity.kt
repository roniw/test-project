package id.test.myapplication

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.CountDownTimer
import android.view.View
import kotlinx.android.synthetic.main.activity_splash.*

class SplashActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
        val timer = object: CountDownTimer(5000, 1000) {
            override fun onTick(millisUntilFinished: Long) {

                tvCount.text = "${(millisUntilFinished.toInt()/1000) }"
            }

            override fun onFinish() {
                tvCount.visibility = View.GONE
                var i = Intent(this@SplashActivity,MainActivity2::class.java)
                if(!prefs.loginState!!){
                    i = Intent(this@SplashActivity,LoginActivity::class.java)
                }

                startActivity(i)
                finish()
            }
        }
        timer.start()
    }
}