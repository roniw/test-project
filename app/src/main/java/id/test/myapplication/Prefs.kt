package id.test.myapplication

import android.content.Context
import android.content.SharedPreferences

class Prefs(context: Context){
    private val MODE = Context.MODE_PRIVATE
    private val PREFS_FILENAME = "app_prefs"
    private val prefs: SharedPreferences = context.getSharedPreferences(PREFS_FILENAME, MODE)
    private val USER = "username"
    private val PASS = "password"
    private val LOGIN_STATE = "login_state"

    fun deletePrefs(){
        prefs.edit().clear().commit()
    }
    var loginState : Boolean?
        get() = prefs.getBoolean(LOGIN_STATE, false)
        set(value) = prefs.edit().putBoolean(LOGIN_STATE, value!!).apply()

    var username : String?
        get() = prefs.getString(USER, "")
        set(value) = prefs.edit().putString(USER, value).apply()

    var password : String?
        get() = prefs.getString(PASS, "")
        set(value) = prefs.edit().putString(PASS, value).apply()
}