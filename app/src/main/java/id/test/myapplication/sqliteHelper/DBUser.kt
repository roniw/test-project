package id.test.myapplication.sqliteHelper

import android.content.ContentValues
import android.content.Context

class DBUser(context: Context): DBBaseHelper(context) {
    companion object{
        private val TBL_USER = "user_table"
        private val KEY_TBL_USER_ID = "id"
        private val KEY_USER = "user"
        private val KEY_PASS = "password"
    }

    fun insertData(user:String,passw:String){
        val db = openDb()
        val values = ContentValues()
        values.put(KEY_USER, user)
        values.put(KEY_PASS, passw)
        db.insert(TBL_USER,null, values)

        db.close()
    }

    fun countDataTemp(user:String,passw:String):Int{
        val countQuery = "SELECT * FROM ${TBL_USER} WHERE ${KEY_USER} = '$user' AND ${KEY_PASS} = '$passw'"
        val db = this.openDb()
        val c = db.rawQuery(countQuery, null)

        val row = c.count
//        Log.e("setChecked3a",row.toString())
        db.close()
        c.close()
        return row
    }
}