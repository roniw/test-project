package id.test.myapplication.sqliteHelper

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper

open class DBBaseHelper {
    companion object {
        private val TAG = "DBBaseAdapter"
        protected val DATABASE_NAME: String = "testDB"
        protected val DATABASE_VERSION: Int = 1

        //===============User Table=======================
        private val TBL_USER = "user_table"
        private val KEY_TBL_USER_ID = "id"
        private val KEY_USER = "user"
        private val KEY_PASS = "password"

        private val CREATE_TABLE_USER = ("CREATE TABLE " + TBL_USER + "("
                + KEY_TBL_USER_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                + KEY_USER + " TEXT, "
                + KEY_PASS + " TEXT " + ")")

    }
    protected var mContext: Context? = null
    protected var mDbHelper: DatabaseHelper? = null

    constructor(context: Context){
        mContext=context
    }

    fun openDb(): SQLiteDatabase {
        if (mDbHelper == null) {
            mDbHelper = DatabaseHelper(mContext!!)
        }
        return mDbHelper!!.getWritableDatabase()
    }

//    fun closeDb() {
//        mDbHelper!!.close()
//    }

    protected class DatabaseHelper(context: Context) : SQLiteOpenHelper(context, DATABASE_NAME , null, DATABASE_VERSION) {

        override fun onCreate(db: SQLiteDatabase) {

            db.execSQL(CREATE_TABLE_USER)

        }

        override fun onUpgrade(db: SQLiteDatabase, oldVersion: Int, newVersion: Int) {

            db.execSQL("DROP TABLE IF EXISTS $TBL_USER")

            onCreate(db)
        }
    }

}