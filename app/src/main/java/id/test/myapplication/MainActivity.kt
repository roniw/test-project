package id.test.myapplication

import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.FragmentTransaction
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import id.test.myapplication.Adapter.DataListAdapter
import id.test.myapplication.Api.Resource
import id.test.myapplication.DataModel.DataResponseModel
import id.test.myapplication.ViewModel.DataViewModel
import id.test.myapplication.ViewModel.getViewModel
import id.test.myapplication.fragment.BtcFragment
import id.test.myapplication.fragment.CatFactFragment
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity() {
    private val dataViewModel by lazy {
        getViewModel<DataViewModel>()
    }
    private val btcFragment = BtcFragment()
    private val catFactFragment = CatFactFragment()
    private val profileFragment = BtcFragment()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        showFragmentBTC()

    }

    protected fun showFragmentBTC(){
        val ft : FragmentTransaction = supportFragmentManager.beginTransaction()
        if (btcFragment.isAdded()){
            ft.show(btcFragment)
        } else{
            ft.add(R.id.mMainFrame, btcFragment, "BTC")
        }
        if (catFactFragment.isAdded) ft.hide(catFactFragment)
        if (profileFragment.isAdded) ft.hide(profileFragment)
        ft.commit()
    }

    protected fun showFragmentCat(){
        val ft : FragmentTransaction = supportFragmentManager.beginTransaction()
        if (catFactFragment.isAdded()){
            ft.show(catFactFragment)
        } else{
            ft.add(R.id.mMainFrame, catFactFragment, "CAT")
        }
        if (btcFragment.isAdded) ft.hide(btcFragment)
        if (profileFragment.isAdded) ft.hide(profileFragment)
        ft.commit()
    }

    protected fun showFragmentProfile(){
        val ft : FragmentTransaction = supportFragmentManager.beginTransaction()
        if (profileFragment.isAdded()){
            ft.show(profileFragment)
        } else{
            ft.add(R.id.mMainFrame, profileFragment, "PROFILE")
        }
        if (catFactFragment.isAdded) ft.hide(catFactFragment)
        if (btcFragment.isAdded) ft.hide(btcFragment)
        ft.commit()
    }

}