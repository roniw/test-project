package id.test.myapplication

class EndPoint {
    companion object{
        val BTC_URL_API = "https://api.coindesk.com/v1/bpi/currentprice.json"
        val CAT_FACT_API = "https://catfact.ninja/fact"
    }
}