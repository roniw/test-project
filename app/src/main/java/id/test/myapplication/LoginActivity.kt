package id.test.myapplication

import android.content.Intent
import android.os.Bundle
import android.view.Window
import android.view.WindowManager
import androidx.appcompat.app.AppCompatActivity
import id.test.myapplication.sqliteHelper.DBUser
import kotlinx.android.synthetic.main.activity_login.*

class LoginActivity : AppCompatActivity() {

    var userValid = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
//        setActionBar(loginToolbar)
        val dbUser = DBUser(this@LoginActivity)
        textReg2.setOnClickListener {
            val i = Intent(this,RegisterActivity::class.java)
            startActivity(i)
        }

        btnLogin.setOnClickListener {
            if (etUsername.text.isNullOrEmpty()){
                etUsername.setError("Username should not be empty!!")
            }

            if (etPassword.text.isNullOrEmpty()){
                etPassword.setError("Password should not be empty!!")
            }

            if (!etPassword.text.isNullOrEmpty()&&!etPassword.text.isNullOrEmpty()){

                val dataCount = dbUser.countDataTemp(etUsername.text.toString(),etPassword.text.toString())
                if ((etUsername.text.toString() == "admin" && etPassword.text.toString() == "2323") ||
                        dataCount == 1){
                    prefs.loginState = true
                    prefs.username = etUsername.text.toString()
                    prefs.password = etPassword.text.toString()
                    val i = Intent(this,MainActivity2::class.java)
                    startActivity(i)
                    finish()
                }else{
                    etUsername.setError("Username not registered or invalid password")
                }
            }

        }
    }

    override fun onSupportNavigateUp(): Boolean {
        finishAffinity()
        return true
    }
}