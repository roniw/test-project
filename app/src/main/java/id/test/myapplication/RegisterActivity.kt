package id.test.myapplication

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import id.test.myapplication.sqliteHelper.DBUser
import kotlinx.android.synthetic.main.activity_register.*

class RegisterActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)

        btnReg.setOnClickListener {
            if (etUsername.text.toString().isNullOrEmpty()){
                etUsername.setError("Username should not be empty!!")
            }
            if (etPassword.text.toString().isNullOrEmpty()){
                etUsername.setError("Password should not be empty!!")
            }
            if (!checkboxTerm.isChecked){
                checkboxTerm.setError("Check the term and condition")
            }

            if (!etUsername.text.toString().isEmpty()
                && !etPassword.text.toString().isEmpty()
                && checkboxTerm.isChecked
            ){
                val dbUser = DBUser(this@RegisterActivity)
                dbUser.insertData(etUsername.text.toString(),etPassword.text.toString())

                val i = Intent(this@RegisterActivity,LoginActivity::class.java)
                startActivity(i)
                finish()
            }
        }
    }
}