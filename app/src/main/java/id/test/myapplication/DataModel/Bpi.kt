package id.test.myapplication.DataModel

data class Bpi(
    val EUR: EUR,
    val GBP: GBP,
    val USD: USD
)