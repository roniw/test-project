package id.test.myapplication.DataModel

data class BtcResponseModel(
    val bpi: Bpi,
    val chartName: String,
    val disclaimer: String,
    val time: Time
)