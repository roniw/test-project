package id.test.myapplication.DataModel

data class Time(
    val updated: String,
    val updatedISO: String,
    val updateduk: String
)