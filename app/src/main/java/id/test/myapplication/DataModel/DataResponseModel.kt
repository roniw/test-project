package id.test.myapplication.DataModel

data class DataResponseModel(
    val fact: String,
    val length: Int
)