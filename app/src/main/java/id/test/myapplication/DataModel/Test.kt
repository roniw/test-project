package id.test.myapplication.DataModel

//{
//    "time": {
//    "updated": "May 24, 2022 03:10:00 UTC",
//    "updatedISO": "2022-05-24T03:10:00+00:00",
//    "updateduk": "May 24, 2022 at 04:10 BST"
//},
//    "disclaimer": "This data was produced from the CoinDesk Bitcoin Price Index (USD). Non-USD currency data converted using hourly conversion rate from openexchangerates.org",
//    "chartName": "Bitcoin",
//    "bpi": {
//    "USD": {
//    "code": "USD",
//    "symbol": "&#36;",
//    "rate": "29,272.7691",
//    "description": "United States Dollar",
//    "rate_float": 29272.7691
//},
//    "GBP": {
//    "code": "GBP",
//    "symbol": "&pound;",
//    "rate": "23,439.7893",
//    "description": "British Pound Sterling",
//    "rate_float": 23439.7893
//},
//    "EUR": {
//    "code": "EUR",
//    "symbol": "&euro;",
//    "rate": "27,705.4758",
//    "description": "Euro",
//    "rate_float": 27705.4758
//}
//}
//}