package id.test.myapplication.fragment

import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import id.test.myapplication.LoginActivity
import id.test.myapplication.MainActivity2
import id.test.myapplication.R
import id.test.myapplication.prefs
import kotlinx.android.synthetic.main.fragment_profile.*

class ProfileFragment : Fragment() {


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_profile, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        tvUsername.text = prefs.username!!
        btnLogout.setOnClickListener {
            prefs.deletePrefs()
            prefs.loginState = false
            val i = Intent(requireActivity(), LoginActivity::class.java)
            startActivity(i)
            requireActivity().finish()
        }
    }
}