package id.test.myapplication.fragment

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import id.test.myapplication.Adapter.DataListAdapter
import id.test.myapplication.Api.Resource
import id.test.myapplication.DataModel.BtcResponseModel
import id.test.myapplication.DataModel.DataResponseModel
import id.test.myapplication.R
import id.test.myapplication.ViewModel.DataViewModel
import id.test.myapplication.ViewModel.getViewModel
import kotlinx.android.synthetic.main.fragment_btc.*
import kotlinx.android.synthetic.main.fragment_cat_fact.*

class BtcFragment : Fragment() {
    private val dataViewModel by lazy {
        getViewModel<DataViewModel>()
    }
    private val TAG = "AppTag"

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_btc, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        dataViewModel.btcDataGet.observe(requireActivity(),getDataObserver)
        dataViewModel.getDataBTC()
    }

    private val getDataObserver =
        Observer<Resource<BtcResponseModel>> { value -> value?.let {
            when (it.status) {
                Resource.Status.SUCCESS -> {
                    Log.d(TAG, "onSuccess")
                    val data = it.data
                    updateTime.text = "Update : ${data!!.time.updated}"
                    updatedISO.text = "ISO : ${data!!.time.updatedISO}"
                    updatedUK.text = "UK : ${data!!.time.updateduk}"

                    chartName.text = data!!.chartName
                    usdVal.text = "${data.bpi.USD.code} : ${data.bpi.USD.rate}"
                    gbpVal.text = "${data.bpi.GBP.code} : ${data.bpi.GBP.rate}"
                    eurVal.text = "${data.bpi.EUR.code} : ${data.bpi.EUR.rate}"
                }
                Resource.Status.ERROR -> {
                    Log.d(TAG, "onError")
                }
                Resource.Status.LOADING -> {
                    Log.d(TAG, "onLoading")
                }
            }
        } }

}