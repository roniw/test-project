package id.test.myapplication.fragment

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import id.test.myapplication.Adapter.DataListAdapter
import id.test.myapplication.Api.Resource
import id.test.myapplication.DataModel.DataResponseModel
import id.test.myapplication.R
import id.test.myapplication.ViewModel.DataViewModel
import id.test.myapplication.ViewModel.getViewModel
import kotlinx.android.synthetic.main.fragment_cat_fact.*


class CatFactFragment : Fragment() {
    private val dataViewModel by lazy {
        getViewModel<DataViewModel>()
    }
    var listData = ArrayList<DataResponseModel>()
    private val TAG = "AppTag"

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_cat_fact, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        dataViewModel.dataGet.observe(requireActivity(),getDataObserver)
        btnTest.setOnClickListener {
            dataViewModel.getData()
        }
    }

    private val getDataObserver =
        Observer<Resource<DataResponseModel>> { value -> value?.let {
            when (it.status) {
                Resource.Status.SUCCESS -> {
                    Log.d(TAG, "onSuccess")
                    val data = it.data

                    for (i in 1..2){
                        val dataSingle = DataResponseModel(data!!.fact,data.length + i)
                        listData.add(dataSingle)

                    }
                    val adapter = DataListAdapter(requireActivity(),listData)
                    rvList.layoutManager = LinearLayoutManager(requireActivity(), LinearLayoutManager.VERTICAL, false)
                    rvList.addItemDecoration(
                        DividerItemDecoration(
                            rvList.getContext(),
                            DividerItemDecoration.VERTICAL
                        )
                    )
                    rvList.adapter = adapter
                }
                Resource.Status.ERROR -> {
                    Log.d(TAG, "onError")
                }
                Resource.Status.LOADING -> {
                    Log.d(TAG, "onLoading")
                }
            }
        } }
}