package id.test.myapplication.ViewModel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import id.test.myapplication.Api.Api
import id.test.myapplication.Api.Resource
import id.test.myapplication.DataModel.BtcResponseModel
import id.test.myapplication.DataModel.DataResponseModel
import io.reactivex.disposables.CompositeDisposable



class DataViewModel : ViewModel(){
    val compositeDisposable = CompositeDisposable()
    val dataGet = MutableLiveData<Resource<DataResponseModel>>()
    val btcDataGet = MutableLiveData<Resource<BtcResponseModel>>()

    fun getDataBTC(){
        this.dataGet.value = Resource.loading(null)
        val disposable = Api.getDataBTC().subscribe(this::onSuccessBtcResponse, this::onErrorBtcResponse)
        compositeDisposable.add(disposable)
    }
    private fun onSuccessBtcResponse(response: BtcResponseModel) {
        this.btcDataGet.value = Resource.success(response)
    }
    private fun onErrorBtcResponse(throwable: Throwable) {
        btcDataGet.value = Resource.error(throwable)
    }

    fun getData(){
        this.dataGet.value = Resource.loading(null)
        val disposable = Api.getData().subscribe(this::onSuccessResponse, this::onErrorResponse)
        compositeDisposable.add(disposable)
    }
    private fun onSuccessResponse(response: DataResponseModel) {
        this.dataGet.value = Resource.success(response)
    }
    private fun onErrorResponse(throwable: Throwable) {
        dataGet.value = Resource.error(throwable)
    }


}