package id.test.myapplication

import android.app.Application
import android.content.Context
import androidx.multidex.MultiDex
import id.test.myapplication.Prefs

val prefs: Prefs by lazy {
    AppPrefs.prefs!!
}

class AppPrefs : Application(){
    companion object {
        var prefs: Prefs? = null
    }

    override fun onCreate() {
        super.onCreate()
        prefs = Prefs(applicationContext)
    }

    override fun attachBaseContext(base: Context?) {
        super.attachBaseContext(base)
        MultiDex.install(this)

    }
}