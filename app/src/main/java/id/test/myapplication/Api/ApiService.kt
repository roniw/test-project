package id.test.myapplication.Api

import id.test.myapplication.DataModel.BtcResponseModel
import id.test.myapplication.DataModel.DataResponseModel
import io.reactivex.Observable
import retrofit2.http.*

interface ApiService {

///// GET
    @GET
    fun dataGet(@Url url : String):Observable<DataResponseModel>

    @GET
    fun dataGetBTC(@Url url : String):Observable<BtcResponseModel>
}